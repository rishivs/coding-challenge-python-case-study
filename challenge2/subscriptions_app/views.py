from django.conf import settings
from django.http.response import JsonResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import TemplateView
from django_registration.forms import RegistrationForm
from .models import UserProfile, Subscription
from django.contrib.auth import login
import stripe

stripe.api_key = settings.STRIPE_SECRET_KEY


class HomePageView(TemplateView):
    """
    Index page views return stripe public key.
    """

    template_name = "index.html"

    def get_context_data(self, **kwargs):
        """
        Create subscription in db after user registration
        """

        context = super().get_context_data(**kwargs)
        context["key"] = settings.STRIPE_PUBLISHABLE_KEY

        if self.request.user.is_authenticated:
            """
            Check user is authenticated and has at least
            one subscription (Trial or Premium) and then
            update it to subscription model.
            """

            customer_id=self.request.user.userprofile.stripe_user_id
            get_subscription=stripe.Subscription.list(limit=1, customer=customer_id)
            try:
                if get_subscription['data'][0].trial_end:
                    state = 'TRIAL_MEMBER'
                else:
                    state = 'PREMIUM_MEMBER'
                create_subs = Subscription.objects.update_or_create(
                    user=self.request.user, state=state, stripe_subs_id=get_subscription['data'][0].id
                )
                context["subscriptions"] = state
            except Exception as e:
                context["message"] = "Please Buy Subscription for complete registration"

        return context


class SuccessView(TemplateView):
    """
    Show sucess template after buy a subscription.
    """
    
    template_name = "success.html"


class CancelledView(TemplateView):
    """
    Show cancel template after cancel the subscription.
    """

    template_name = "cancelled.html"


def create_user(request, state):
    """
    Create customer on database and strpie dashboard.
    """
    
    price_id = "price_1IO1DcAQAJmuHcD4iz6gyhOs"
    if request.method == "POST":
        user_form = RegistrationForm(request.POST)
        if user_form.is_valid():
            user = user_form.save()
            stripe_data = stripe.Customer.create(
                email=user.email,
                name=user.username,
            )
            user_profile = UserProfile.objects.create(
                user=user, stripe_user_id=stripe_data.id
            )
            login(request, user)
            if state == "trial_member":
                """
                Check regisration type and create
                a trial subscription for the user and 
                store it on both stipe and local database.
                """
                
                subscription = stripe.Subscription.create(
                    customer=stripe_data.id,
                    items=[
                        {
                            "price": price_id,
                            "quantity": 1,
                        },
                    ],
                    trial_period_days=7,
                )
                create_subs = Subscription.objects.create(
                    user=user, state='TRIAL_MEMBER', stripe_subs_id=subscription.id
                )
                return render(
                    request, "index.html",
                    {
                        "message": "You Got 7 Days Free Trial Membership"
                    }
                )
            elif state == "premium_member":
                """
                Check registraion type to 'premium_member',
                and redirect user to complete the payment process
                on index page.
                """
                
                return render(
                    request, "index.html",
                    {
                        "success": True,
                        "message": "Please Buy Subscription for continue registration"
                    }
                )
    else:
        user_form = RegistrationForm()
    return render(
        request,
        "django_registration/registration_form.html",
        {"form": user_form, "state": state},
    )


@csrf_exempt
def stripe_config(request):
    """
    Return stripe public key in json format.
    """
    
    if request.method == "GET":
        stripe_config = {"publicKey": settings.STRIPE_PUBLISHABLE_KEY}
        return JsonResponse(stripe_config, safe=False)


@csrf_exempt
def create_checkout_session(request):
    """
    Create subscription checkout session for premium membership.
    """

    if request.method == 'GET':
        domain_url = 'http://localhost:8000/'
        stripe.api_key = settings.STRIPE_SECRET_KEY
        price_id = "price_1IO1DcAQAJmuHcD4iz6gyhOs"
        customer_id = request.user.userprofile.stripe_user_id
        try:
            # Create new Checkout Session for the premium subscription

            checkout_session = stripe.checkout.Session.create(
                success_url=domain_url + "success?session_id={CHECKOUT_SESSION_ID}",
                cancel_url="{0}cancelled/".format(domain_url),
                payment_method_types=['card'],
                customer=customer_id,
                mode="subscription",
                line_items=[
                    {
                        "price": price_id,
                        "quantity": 1,
                    }
                ]
            )
            return JsonResponse({'sessionId': checkout_session['id']})
        except Exception as e:
            return JsonResponse({'error': str(e)})
