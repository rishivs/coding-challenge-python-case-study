from django.conf import settings
from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser


class UserProfile(AbstractBaseUser):
    """
    Create User profile model for add new field stipe user id.
    """

    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    stripe_user_id = models.CharField(max_length=64, null=False, blank=False)

    def __str__(self):
        return self.stripe_user_id


class Subscription(models.Model):
    """
    Crate subscription model for store subscription
    state and subscription id.
    """

    SUBSCRIPTION_STATE = (
        ("TRIAL_MEMBER", "trial_member"),
        ("PREMIUM_MEMBER", "premium_member"),
    )

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    state = models.CharField(
        max_length=64, choices=SUBSCRIPTION_STATE, null=False, blank=False
    )
    stripe_subs_id = models.CharField(max_length=64, null=False, blank=False)
