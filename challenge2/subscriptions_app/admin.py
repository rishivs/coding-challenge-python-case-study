from django.contrib import admin
from .models import UserProfile, Subscription


class UserProfileAdmin(admin.ModelAdmin):
    # Show user profile fields in Admin panel.

    fields = ("user", "stripe_user_id")


class SubscriptionAdmin(admin.ModelAdmin):
    # Show subscription fields on Admin panel.

    fields = ("state", "user", "stripe_subs_id")


admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Subscription, SubscriptionAdmin)
