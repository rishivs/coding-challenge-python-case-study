"""
Challenge 1:
Problem Overview: Network Failure Point
    We have a mesh network connected by routers labeled
    from 1 to 6 in a directed manner.
    Write an algorithm that can detect the routers with
    the highest number of connections so we might know which
    routers will cause a network failure if removed.
    Multiple connections between the same routers should be
    treated as separate connections.
    A tie for the most number of connections is possible.

Requirements:
    Implement a identify_router function that accepts an input
    graph of nodes representing the total network and identifies
    the node with the most number of connections. 
    Return the label of the node.

    Implement a directed graph data structure using Python 3.6 and up.
    Each node is unique thus there will be no cases of having
    multiple nodes with the same label.
    Each node will have an infinite number of both inbound and outbound links.

Test Cases:
    1 -> 2 -> 3 -> 5 -> 2 -> 1 = 2
    *since router 2 has 2 inbound links and 2 outbound links

    1 -> 3 -> 5 -> 6 -> 4 -> 5 -> 2 -> 6 = 5
    *since router 5 has 2 inbound links and 2 outbound link

    2 -> 4 -> 6 -> 2 -> 5 -> 6 = 2, 6
    *since router 2 has 1 inbound link and 2 outbound links and
    6 has 2 inbound links and 1 outbound link
"""

# Note: please install networkx with following version.
# networkx==2.5

import networkx as nx


g = nx.DiGraph()
g.add_nodes_from([1,2,3,4,5,6])

def identify_router(graph_nodes):
    """
    Get graph nodes as string and added them to DiGraph edges.
    """

    node_list = graph_nodes.split(' -> ')
    get_dict = {}
    get_value = ""
    result = {}

    for node in range(len(node_list)):
        # added edges to DiGraph from the list of nodes.

        if node < len(node_list)-1:
            g.add_edge(node_list[node], node_list[node+1]) 
    
    for key in g._pred.keys():
        """
        Get inbound values and updated with nodes in the 
        get_dict with the sum of inbound and outbound links.
        """

        if len(g._pred[key]) > 1 :
            get_dict.update({int(key):len(g._pred[key])+len(g._adj[key])})

    for key in g._adj.keys():
        """
        Get outbound values and updated with nodes in the 
        get_dict with the sum of inbound and outbound links.
        """

        if len(g._adj[key]) > 1:
            get_dict.update({int(key):len(g._pred[key])+len(g._adj[key])})

    g.clear() # clear graph edges.

    if len(get_dict) > 1:
        for node in get_dict:
            """
            Set result for the node which has maximum number
            of inbound and outbound links.
            """

            if get_value != "":
                if get_dict[node] > get_value:
                    get_value = get_dict[node]
                    result.update({node:get_value})
                elif get_dict[node] == get_value:
                    get_value = get_dict[node]
                    result.update({node:get_value})
            else:
                get_value = get_dict[node]
                result.update({node:get_value})

        return list(set(result))
    else:
        return list(set(get_dict))

def test_graph():
    """
    Write test for check functionality of identify_router function.
    """

    assert identify_router('1 -> 2 -> 3 -> 5 -> 2 -> 1') == [2]
    assert identify_router('1 -> 3 -> 5 -> 6 -> 4 -> 5 -> 2 -> 6') == [5]
    assert identify_router('2 -> 4 -> 6 -> 2 -> 5 -> 6') == [2,6]

if __name__ == "__main__":
    test_graph()
    print("Everything passed")
