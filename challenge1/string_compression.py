"""
Challenge 1: Algorithm Test
    Problem Overview: String Compression
    Implement a string compression using python. For example, aaaabbbccddddddee would become a4b3c2d6e2. If the length of the string is not reduced, return the original string.

Requirements:
    Implement a compress function which accepts an input string and returns a compressed string. Code must be implemented using python 3.6 and must follow strictly pep8 rules.
    Provide comments regarding the implementation.

Test Cases:
    assert compress('bbcceeee') == 'b2c2e4'
    assert compress('aaabbbcccaaa') == 'a3b3c3a3'
    assert compress('a') = a

Explanation:
    Explain time complexity of the compression written.
"""

# The solution below should yield us with a Time and Space complexity of O(n)

def compress(string):
    """
    This solution provide string compression.
    If the length of the string is not reduced,return the original string.
    """

    # Create empty result string and get the length of string.
    result = ""
    length = len(string)

    # Check for length 0 and return empty sting.
    if length == 0:
        return ""

    # Check for length 1 and return string.
    if length == 1:
        return string

    #Intialize Values
    last_element = string[0]
    count = 1
    index = 1

    while index < length:

        # Check to see if it is the same letter
        if string[index] == string[index - 1]: 
            # Add a count if same as previous
            count += 1
        else:
            if count <= 1:
                """
                Check length of string is less or equal to 1
                then return original string for center elements.
                """
                result = result + string[index - 1]
            else:
                # Otherwise store the previous data
                result = result + string[index - 1] + str(count)
            count = 1

        # Add to index count to terminate while loop
        index += 1

    # Put everything back into run
    if count <= 1:
        """
        Check length of string is less or equal to 1
        then return original string for last elements.
        """
        result = result + string[index - 1]
    else:
        # Otherwise store the previous data
        result = result + string[index - 1] + str(count)
    return result


def test_sum():
    """
    Write test for check functionality of compress function.
    """

    assert compress('bbcceeee') == 'b2c2e4'
    assert compress('aaabbbcccaaa') == 'a3b3c3a3'
    assert compress('a') == 'a'

if __name__ == "__main__":
    test_sum()
    print("Everything passed")
